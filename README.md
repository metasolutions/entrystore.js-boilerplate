# Store boilerplate

The purpose of this repository is to have a repository to copy when writing scripts to interact with an EntryStore installation.

## Base a new repository on this one
Assuming you want to create a new repository named banana you need to:
* Change in package.json so the name and descriptions are correct.
* Change in bin/init.js, in the two places where it says "boilerplate", change to "banana".


## Install
Make sure you have npm installed. Do the following:

    npm install
    cp config.js_example config.js

Set the correct user and password to the EntryStore instance in the config.js.

## Running example scripts

    cd examples
    ./run import 4

You have to change the number 4 to the project id you want to import to. (The technical term for a project is context and it is visible in the URL when you enter a project in the EntryScape UI.)

If you want to clean up, you can run the script `rm`, this command also takes a project id as parameter, i.e.:

    cd examples
    ./run rm 4
    
## Preparing another folder with scripts

In each folder there is a run (and potentially a start script). The only you need to do is to copy those over from another place (e.g. the example directory) and change the name of the folder in the scripts. In the start script there is also a prefix of the log file that you typically change.