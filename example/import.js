define([
  'store/EntryStore',
  'config',
], (EntryStore, config) => {
  let contextId;

  if (process.argv.length > 3) {
    contextId = process.argv[3];
  } else {
    console.log('Required parameters: contextid');
    process.exit();
    return;
  }

  const rows = [
    { name: 'Testdata 1' },
    { name: 'Testdata 2' },
  ];

  const es = new EntryStore(config.repository);
  es.getAuth().login(config.user, config.password).then(() => {
    const context = es.getContextById(contextId);
    // Add entries to context.
    rows.forEach((row) => {
      context.newNamedEntry()
        .addL('skos:prefLabel', row.name)
        .add('rdf:type', 'foaf:Document')
        .commit()
        .then((entry) => {
          console.log(`Added entry with URI: ${entry.getURI()}`);
        });
    });
  });
});
