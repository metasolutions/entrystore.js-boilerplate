define([
  'store/EntryStore',
  'config',
], (EntryStore, config) => {
  let ctxtid;
  if (process.argv.length > 3) {
    ctxtid = process.argv[3];
  }

  if (ctxtid == null) {
    console.log('You need to provide the context id from where to remove stuff');
  } else {
    const es = new EntryStore(config.repository);
    es.getAuth().login(config.user, config.password).then(() => {
      const toDelete = [];
      es.newSolrQuery().title('*').context(es.getContextById(ctxtid)).list()
        .forEach((entry) => {
          toDelete.push(entry);
        })
        .then(() => {
          toDelete.forEach((e) => {
            console.log(`Deleting entry with URI: ${e.getURI()}`);
            e.del();
          });
        });
    });
  }
});
