const cmd = (process.argv[2] || '').replace('.js', '');
if (cmd[cmd.length - 1] === '/') {
  console.log('You need to specify something to run');
  process.exit(0);
}
dojoConfig = { locale: 'en' };// to run validate, which requires locale
const requirejs = require('requirejs');

requirejs.config({
  nodeRequire: require,
  baseUrl: '../node_modules',
  packages: [
    {
      name: 'boilerplate',
      location: '..',
    },
    {
      name: 'config',
      location: '..',
      main: 'config.js',
    },
    {
      name: 'md5',
      location: 'blueimp-md5/js',
      main: 'md5.min',
    },
  ],
  map: {
    'store/rest': {
      'dojo/request': 'dojo/request/node', // Force using node
    },
  },
  deps: ['store/config/fix', `boilerplate/${cmd}`],
});
